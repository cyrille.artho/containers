# README #

## To compile: ./compile.sh ##

This assumes you either have Scala 2.9 installed, or a version of
Modbat in /usr/local/java/modbat.jar that matches your Scala
installation.

## To run: ./run.sh ##

Currently the tests are configured to stop after one failure. With
the given random seed, three tests are produced, with the last one
being a failure.
