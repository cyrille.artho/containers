#!/bin/sh
. modbat.sh
time scala -classpath lib/experiment.jar \
        ${MODBAT} \
        -s=1 \
        -n=1000 \
        --abort-probability=0.02 \
        --print-stack-trace \
        --auto-labels \
	--no-redirect-out \
        modbat.tu.TreeMapModel
