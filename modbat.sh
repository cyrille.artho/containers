#!/bin/sh
SCALA_VERSION="`scala -version 2>& 1 | sed -e 's/.*version \([0-9.-]*\).*/\1/'`"
case $SCALA_VERSION in
2.9*)
  MODBAT=lib/modbat-1.0.1.jar # compatible with Scala 2.9
  ;;
*)
  MODBAT=/usr/local/java/modbat.jar # TODO: detect modbat (check if MODBAT set)
  ;;
esac
