package modbat.tu

import modbat.dsl._
import Containers.limit
import scala.collection.JavaConversions._

import experiment.util.TreeSet
import experiment.util.Set
import java.util.Set
import java.util.TreeSet

class TreeSetModel extends HashSetModel {

  //override val testData = new experiment.util.TreeSet() // for the faulty version (TU library)
  override val testData = new java.util.TreeSet[Object]() // for the correct version (Java library)

  def first() = {
    require(n > 0)
    var res = testData.first()

    var min = data(0)
    for(i <- 0 to n-1) {
      val el = data(i)
      if (el.asInstanceOf[Comparable[Object]].compareTo(min) < 0) {
        min = el
      }
    }
    assert(res == min)
  }

//  def last() = data.toList.max


  def last() = {
    require(n > 0)
    var res = testData.last()
    
    var max = data(0)
    for(i <- 0 to n-1) {
      val el = data(i)
      if (el.asInstanceOf[Comparable[Object]].compareTo(max) > 0) {
        max = el
      }
    }
    assert(res == max)
  }

  def noSuchElement {
    require(n == 0)
    choose(
      { () => testData.last() },
      { () => testData.first() }
    )
  }


  //def last = { // compare the largest element in the array with the result }

  "main" -> "main" := first
  "main" -> "main" := last
  "main" -> "main" := noSuchElement throws("NoSuchElementException")
//      choose(
/*
          { () => iterator() }
          { () => getMatch() }
*/
//      )
}

