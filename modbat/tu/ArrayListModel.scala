package modbat.tu

import modbat.dsl._
import Containers.limit
import scala.collection.JavaConversions._

import experiment.util.ArrayList
import java.util.ArrayList

class ArrayListModel extends ListModel {
  //override val testData = new experiment.util.ArrayList() // for the faulty version (TU library)
  override val testData = new java.util.ArrayList[Object]() // for the correct version (Java library)
}
