package modbat.tu

import modbat.dsl._
import Containers.limit
import scala.collection.JavaConversions._


import experiment.util.HashMap
import java.util.HashMap


class HashMapModel extends MapModel {

  //override val testData = new experiment.util.HashMap() // for the faulty version (TU library)
  override val testData = new java.util.HashMap[Object,Object]() // for the correct version (Java library)

  "main" -> "main" := put(new Integer(choose(0,20)),new Integer(choose(0,5))) weight 10
  "main" -> "main" := size
  "main" -> "main" := remove(new Integer(choose(0,20))) label "removeWithKey"
  "main" -> "main" := containsKey(new Integer(choose(0,20)))
  "main" -> "main" := clear
  "main" -> "main" := get(new Integer(choose(0,20)))
  "main" -> "main" := keySet
  "main" -> "main" := values
  //"main" -> "main" := noSuchElement throws("NoSuchElementException")
//      choose(
/*
          { () => entrySet() }
*/
//      )
}

