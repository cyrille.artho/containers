package modbat.tu

import modbat.dsl._
import Containers.limit
import scala.collection.JavaConversions._

import experiment.util.LinkedList
import java.util.LinkedList

class LinkedListModel extends ListModel {

  //override val testData = new experiment.util.LinkedList() // for the faulty version (TU library)
  override val testData = new java.util.LinkedList[Object]() // for the correct version (Java library)
}
