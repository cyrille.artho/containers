package modbat.tu

import modbat.dsl._
import Containers.limit
import scala.collection.JavaConversions._

import java.util.Stack
import experiment.util.Stack

class StackModel extends Model {
  //val testData = new experiment.util.Stack() // for the faulty version (TU library)
  val testData = new java.util.Stack[Object]() // for the correct version (Java library)

  var data = new Array[Object](Containers.limit)
  var isUsed = new Array[Boolean](Containers.limit)
  var n = 0
  var hwm = 0
  //var iterators = new Array[Iterator[AbstractContainer]]()
  var nIt = 0
  // TODO: Add new (unique) version number (trait) that checks if a
  // given structure is the same; use that to check equivalence of clones



  def push(o: Object) = {
    require(n < Containers.limit) // limit state space
    var res = testData.push(o)

    data(n) = o
    //assert (data(n) == res, { "Expected: " + data(n) + ", get: " + res + "." } ) //comment this line in order to go further in the tests
    n += 1
    true
  }

  def peek() {
    require(n != 0)
    var res = testData.peek()
    assert(res == data(n-1), { "Expected: " + data(n-1) + ", get: " + res + "." } )
  }

  def pop() {
    require(n != 0)
    var res = testData.pop()
    assert(res == data(n-1))
    n -= 1
    }

  def stackEmpty() {
    require(n == 0)
    choose(
      { () => testData.pop() },
      { () => testData.peek() }
    )   
  }
   def size() {
    assert(testData.size() == n, {"Expected: " + n + ", get: " + testData.size() + "." } )
  }

  def clear() {
    testData.clear()
    n = 0
  }

  def isEmpty() {
    assert(testData.isEmpty == (n == 0))
  }

  "main" -> "main" := push(new Integer(n)) weight 3
  "main" -> "main" := peek
  "main" -> "main" := pop
  "main" -> "main" := size
  "main" -> "main" := clear
  "main" -> "main" := isEmpty
  "main" -> "main" := stackEmpty throws("EmptyStackException")

//      choose(
/*
          { () => toString() }*/
//      )    )
}

