package modbat.tu

import modbat.dsl._
import Containers.limit

import java.util.AbstractCollection
import java.util.ArrayList
import java.util.TreeSet
import java.util.Set
import java.util.Map
import experiment.util.Map

trait MapModel extends Model {

  //val testData: experiment.util.Map// for the faulty version (TU library)
  val testData: java.util.Map[Object,Object]// for the correct version (Java library)

  var data = new Array[Object](Containers.limit)
  var key = new Array[Object](Containers.limit)
  var isUsed = new Array[Boolean](Containers.limit)
  var n = 0

  def foundKey() {
    
  }

  def put(k: Object, o: Object) = {
    require(n < Containers.limit) // limit state space
    var res = testData.put(k,o)
    var foundKey = false
    var i = 0
    var old: Object = null

    while((foundKey != true) && (i < n)) {
      if(key(i) == k) {
        old = data(i)
        data(i) = o
        foundKey = true
      }
      i += 1
    }

    if (foundKey == false) {
      data(n) = o
      key(n) = k
      n += 1
    }
    if(foundKey == true) {
      assert(res == old , { "Expected value: " + o + " with key: " + k + ", got " + res + "." })
    } else {
      assert(res == null)
    }
  }

  def remove(k: Object) = {
    var res = testData.remove(k)
    var foundKey = false
    var i = 0 
    var old: Object = null

    while((foundKey != true) && (i < n)) {
      if(k == key(i)) {
        old = data(i)
        data(i) = data(n-1)
        key(i) = key(n-1)
        n -= 1
        foundKey = true
      }
      i += 1
    }
    if(foundKey == true) {
      assert(res == old)
    } else {
      assert(res == null)
    }
  } 

  def containsKey(k: Object) {
    var res = testData.containsKey(k)
    var foundKey = false
    var i = 0

    while((foundKey != true) && (i < n)) {
      if(k == key(i)) {
        foundKey = true
      }
      i += 1
    }
    assert(res == foundKey, {"Expected: " + foundKey + ", get: " + res + "." } )
  }

  def get(k: Object) {
    var res = testData.get(k)
    var foundKey = false
    var i = 0
    var valueFound: Object = null
    
    while((foundKey != true) && (i < n)) {
      if(k == key(i)) {
        valueFound = data(i)
        foundKey = true
      }
      i += 1
    }
    if(foundKey == true) {
      assert(res == valueFound, {"Expected: " + valueFound + ", get: " + res + "." } )
    } else {
      assert(res == null, {"Expected: " + null + ", get: " + res + "." } )
    }
  }

/*  def entrySet() {
    var res = testData.entrySet()
    var res2: Set[Object] = new TreeSet()

     
    assert(res.equals(res2), {"Expected: " + res2 + ", get: " + res + "." } )
  }*/

  def keySet() {
    var res = testData.keySet()
    var res2: Set[Object] = new java.util.TreeSet()
    assert (res.size == n, { "Expected " + n + " elements, but got " + res.size } )

    if(n > 0) {
      for(i <- 0 to n-1) {
	assert (res.contains(key(i)), { "Could not find key " + key(i) } )
      }
    }
  }

  def values() {
    //var res: AbstractCollection[Object] = new ArrayList[Object](Containers.limit)
    var res = testData.values.toArray()
    var valu = new Array[Object](n)

//    var seen = new Array[Boolean]

    System.arraycopy(data, 0, valu, 0, n)
    java.util.Arrays.sort(res)
    java.util.Arrays.sort(valu)
    for(i <- 0 to n-1) {
      assert(res(i) == valu(i), {"Expected: " + valu(i) + ", get: " + res(i)  + "." } )
    }
  }

  def size() {
    assert(testData.size() == n, {"Expected: " + n + ", get: " + testData.size() + "." } )
  }

  def clear() {
    testData.clear()
    n = 0
  }

  def isEmpty() {
    assert(testData.isEmpty == (n == 0))
  }

}
