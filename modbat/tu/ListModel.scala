package modbat.tu

import modbat.dsl._
import Containers.limit
import scala.collection.JavaConversions._

import experiment.util.List
import java.util.List
import java.io.Serializable

trait ListModel extends ModelData {
  //override val testData: experiment.util.List// for the faulty version (TU library)
  override val testData: java.util.List[Object]// for the correct version (Java library)

  def invalidateIt() {
/*
    for (it: Iterators) {
      it.invalidate // tell iterator to throw ConcurrentModificationException
      // an alternative would be to use a version number for each container
    }
*/
  }

/*
  def iterator() {
    require(nIt < Containers.itLimit)
    iterators(nIt) = new ...Iterator(this)
    launch(iterators(nIt))
    nIt += 1
  }
*/

  def getFreeSlotFrom(p: Integer) = {
    var i = p
    while (isUsed(i)) {
      i = i + 1
    }
    i
  }


  def add(o: Object) = {
    require(n < Containers.limit) // limit state space
    require(hwm < Containers.limit) // limit state space
    testData.add(o)

//println("Current slot usage: " + isUsed.toList.map(boolToInt).mkString(", "))
    val p = getFreeSlotFrom(hwm)
    data(p) = o
    n += 1
    assert (!isUsed(p))
    isUsed(p) = true
    if (p == hwm) {
      hwm += 1
    }
    invalidateIt
    true
  }

  def getFirstUsedSlotFrom(p: Integer) = {
    var i = p
    while ((i != -1 ) && (!isUsed(i))) {
      i = i - 1
    }
    i+1
  }

  def boolToInt(b: Boolean) = {
    if (b) 1 else 0
  }

  def listIdxToArrayIdx(i: Integer): Integer = {
    var j = 0
    var n = 0
    val l = isUsed.length
    while (j < l) {
      if (isUsed(j)) {
	if (n == i) return j
	n = n + 1
      }
      j = j + 1
    }
    return j
  }

  def remove() {
    require (hwm != 0)
    val i = choose(0, n)
    var ok = false
    val result = (testData.remove(i) != null)
    //println("remove: " + i + ".")
    //println("Current slot usage: " + isUsed.toList.map(boolToInt).mkString(", "))
    val p = listIdxToArrayIdx(i)
    if (isUsed(p)) {
      n -= 1
      isUsed(p) = false
      ok = result
      if (p == hwm-1) {
        hwm = getFirstUsedSlotFrom(hwm-1)
      }
    } else {
      assert (p != hwm)
      ok = !result
    }
    invalidateIt
    assert (ok, { "Remove on container returned " + result + "\n" +
"Remove of " + i + "th el. failed on container with " + testData.size + " elements."
 })
  }

  def outOfBounds {
    choose(
      { () => testData.get(-1) },
      { () => testData.get(n) },
      { () => testData.remove(-1) },
      { () => testData.remove(n) }
    )
  }

  override def clear() {
    testData.clear()
    for ( i <- 0 to hwm-1) {
      isUsed(i) = false
    }
    hwm = 0
    n = 0
    invalidateIt
  }

  def set(o: Object) {
    require (hwm != 0)
    val i = choose(0,n)
    // Check data returned (should be equal to data(p))
    // by calling get
    val old = testData.set(i, o)
   // println("index: " + i + ", changement: " + o + ".")
    val p = listIdxToArrayIdx(i)
    assert(isUsed(p))
    assert(data(p) == old, { "At: " + i + ", expected: " + data(p) + ", got: " + old } )
    data(p) = o
    assert(data(p) == testData.get(i), { "After set at t: " + i + ", expected: " + data(p) + ", got: " + testData.get(i) } )
    invalidateIt
  }

  def remove(o: Object) = {
    var found = false
    val result = testData.remove(o)
    var i = 0
    while ((found == false) && (i < hwm)) {
      if (isUsed(i)) {
        if(data(i) == o) {
          n -= 1
          isUsed(i) = false
          if(i == hwm-1) {
            hwm = getFirstUsedSlotFrom(hwm - 1)
          }
          found = true
        }
      }
      i += 1
    }
    if (found == true) {
      assert(result == true)
    }
    invalidateIt
    true
  }

  def get(i: Integer){
    require (hwm != 0)
    var res = testData.get(i)
    val p = listIdxToArrayIdx(i)
    assert(data(p) == res, { "At " + i + ": Expected: " + data(p) + ", got: " + res + "\n" + "Array list: " + data.foreach(print) + "\n"  } )
  }

  // transitions
  "main" -> "main" := add(new Integer(n)) weight 6 // TODO: call Containers.addToAny instead
  "main" -> "main" := isEmpty
  "main" -> "main" := size
  "main" -> "main" := remove label "removeAt"
  "main" -> "main" := remove(new Integer(choose(0,n))) label "removeEl"
  "main" -> "main" := clear
  "main" -> "main" := set(new Integer(n))
  "main" -> "main" := get(new Integer(choose(0,n)))
  "main" -> "main" := outOfBounds throws("IndexOutOfBoundsException")
//	choose(
/*
	  { () => Containers.addToAny(this) },
	  { () => Containers.anyContains(this) },
	  { () => Containers.equalsToAny(this) },
	  { () => Containers.removeFromAny(this) },
	  { () => Containers.setAny(this) },
	  { () => clone() },
	  { () => hashCode() },
	  { () => iterator() },
	  { () => listIterator() },
	  { () => toArray() },
	  { () => toString() }*/
//	)
}
