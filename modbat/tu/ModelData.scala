package modbat.tu

import modbat.dsl._
import Containers.limit

import java.util.Collection
import experiment.util.Collection

trait ModelData extends Model {

  //val testData: experiment.util.Collection// for the faulty version (TU library)
  val testData: java.util.Collection[Object]// for the correct version (Java library)

  var data = new Array[Object](Containers.limit)
  var isUsed = new Array[Boolean](Containers.limit)
  var n = 0
  var hwm = 0
  //var iterators = new Array[Iterator[AbstractContainer]]()
  var nIt = 0
  // TODO: Add new (unique) version number (trait) that checks if a
  // given structure is the same; use that to check equivalence of clones

  def size() {
    assert(testData.size() == n, {"Expected: " + n + ", get: " + testData.size() + "." } )
  }

  def clear() {
    testData.clear()
    n = 0
  }

  def isEmpty() {
    assert(testData.isEmpty == (n == 0))
  }

}
