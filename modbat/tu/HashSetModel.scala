package modbat.tu

import modbat.dsl._
import Containers.limit
import scala.collection.JavaConversions._

import experiment.util.HashSet
import java.util.HashSet
import experiment.util.Set
import java.util.Set

class HashSetModel extends ModelData {
  //override val testData: experiment.util.Set = new experiment.util.HashSet() // for the faulty version (TU library)
  override val testData: java.util.Set[Object] = new java.util.HashSet[Object]() //for the correct version (Java library)

  def add(o: Object) = {
    require(n < Containers.limit) // limit state space
    var res = testData.add(o)
    
    var ok = true
    for (i <- 0 to n-1) {
      if(o == data(i)) {
        ok = false
      }
    }
    if (ok == true) {
      data(n) = o
      n += 1
    }
    assert(res == ok, { "Expected " + ok + ", got " + res })
  }

  def remove(o: Object) = {
    var res = testData.remove(o)
    var ok = false
    var i = 0
    while((ok != true) && (i < n)) {
      if(o == data(i)) {
        data(i) = data(n-1)
        n -= 1
        ok = true
      }
      i += 1
    }
    assert(res == ok)
  }

  def contains(o: Object) = {
    var res = testData.contains(o)
    var ok = false
    var i = 0
    while((ok != true) && (i < n)) {
      if(o == data(i)) {
        ok = true
      }
      i += 1
    }
    assert(res == ok)
  }

  "main" -> "main" := add(new Integer(choose(0,20))) weight 6
  "main" -> "main" := size
  "main" -> "main" := remove(new Integer(choose(0,20))) label "removeEl"
  "main" -> "main" := contains(new Integer(choose(0,20)))
  "main" -> "main" := clear
//      choose(
/*
          { () => iterator() }
          { () => getMatch() }
*/
//      )
}

